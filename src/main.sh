#!/usr/sbin/awk -f
# Desc: Generate shortened path from input

BEGIN {
   FS="/"
   OFS="/"
}
{
   sub(ENVIRON["HOME"], "~", $0)
   for (i=3;i<NF;i++)
      $i=substr($i,1,1+($i~/^[.]/))(i==1||length($i)<2?"":"*")
}1
