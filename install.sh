#!/usr/bin/bash

function install() {
	printf "%s\n" "Installing path_generator"
	cp -f -p ./src/main.sh "$HOME"/.local/bin/path_generator
}

if [ "${BASH_SOURCE[0]}" -ef "$0" ]; then
	printf "%s\n" "Script is being executed!"
	install
else
	printf "%s\n" "Script is being sourced!"
fi
